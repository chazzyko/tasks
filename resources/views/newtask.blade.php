@extends('layouts.master')
@section('content')
	<div class="row">
		<div class="col-md-6">			
				{{ Form::open(['route'=>'task.store'],['class'=>'form-horizontal task-form']) }}
				<div class="form-group">
					{{ Form::label('task_name',"Task name",['class'=> 'control-label']) }}
					{{Form::text('task_name',null, ['class'=> 'form-control'])}}
					@if ($errors->has('task_name')) <p class="help-block alert alert-danger">{{ 		$errors->first('task_name') }}</p>
					@endif
				</div>
				<div class="form-group">
					{{Form::label('task_content', 'Task content',['class'=> 'control-label'])}}
					{{Form::textarea('task_content',null, ['class'=> 'form-control'])}}
					@if($errors->has('task_content')) <p class="help-block alert alert-danger">{{ $errors->first('task_content') }}</p>
					@endif
				</div>
				<div class="form-group">
					{{Form::submit('Create',['class'=>'btn btn-primary'])}}
				</div>
				{{Form::close()}}
		</div>
	</div>

@endsection