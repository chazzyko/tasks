<!DOCTYPE html>
<html lang="lt">
<head>
	<meta charset="UTF-8">
	<title>@if(isset($title)){!! $title !!}@else Task manager @endif</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">	
	<link rel="stylesheet" type="text/css" href="{!! asset('css/styles.css') !!}">
</head>
<body>
	<div class="container main">
		<header>
			<h1>Task manager</h1>
			<nav>@include('partials.menu')</nav>
		</header>
		@if(session('message'))
			<p class="alert alert-success">{{ session('message') }}</p>
			{{ Session::forget('message') }}
		@endif
		@yield('content')
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="{!! asset('js/main.js') !!}"></script>
</body>
</html>
