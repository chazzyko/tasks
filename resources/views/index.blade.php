@extends('layouts.master')
@section('content')	
	<div class="row">
		<div class="col-md-12 taskLink">
			<p>Check: {!! link_to_route('task.index', 'Task List', $arguments = [], $parameters = []) !!}</p>
		</div>
	</div>
@endsection