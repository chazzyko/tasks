@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-6">
			@if(count($task)>0)
				{!! Form::model($task,['route'=>['task.update',$task->id], 'method'=>'PUT', 'id'=>'taskEditForm', 'class'=>'form-horizontal']) !!}
			<div class="form-group">
				{!! Form::label('task_name', 'Task name', ['class'=>'control-label']) !!}
				{!! Form::text('task_name', null, ['class'=>'form-control']) !!}
				@if($errors->has('task_name')) <p class="help-block alert alert-danger">{!! $errors->first('task_name') !!}</p>@endif
			</div>
			<div class="form-group">
				{!! Form::label('task_content', 'Task content', ['class'=>'control-label'] ) !!}
				{!! Form::textarea('task_content', null, ['class' => 'form-control'] ) !!}
				@if ($errors->has('task_content')) <p class="help-block alert alert-danger">{{ $errors->first('task_content') }}</p>@endif
			</div>
			<div class="form-group">
				{!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
			</div>
				{!! Form::close() !!}				
			@else
				<p>No tasks found.</p>
			@endif
		</div>		
	</div>
@endsection