@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-8">
			@if(count($tasks)>0)
				
					<ul class="list-unstyled taskList">
						@foreach($tasks as $key=>$task)					
							<li><b>{!! ++$count !!}</b><span>{{ link_to_route('task.show',$task->task_name, $arguments = [$task->id] ) }}</span>{{ link_to_route('task.edit','edit',$arguments = [$task->id], $parameters = ['class'=>'btn btn-default taskListEditBtn']) }}							
							</li>
						@endforeach
					</ul>
				
			@else
				<p>No tasks found.</p>
			@endif
			@if($tasks) {{ $tasks->render() }} @endif
		</div>
		
	</div>

@endsection