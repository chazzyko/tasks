{!! link_to('/', 'Home',$arguments = ["class"=>"btn btn-default"]) !!}
{!! link_to_route('task.index', 'All tasks',$arguments = [], $parameters = ["class"=>"btn btn-default"]) !!}
{!! link_to_route('task.create', 'Create new task',$arguments = [], $parameters = ["class"=>"btn btn-default"]) !!}