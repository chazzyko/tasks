@extends('layouts.master')
@section('content')	
	<div class="row oneTask">
		<div class="col-md-5 oneTaskBody">
			@if($task)
				<h3>{{ $task->task_name }}</h3>
				<p class="taskText">{{ $task->task_content }}</p>
				<div class="dates">
					<p><b>Created at:</b> <span>{{ $task->created_at }}</span></p>
					<p><b>Last update:</b> <span>{{ $task->updated_at }}</span></p>
				</div>
				<div class="editBtnGroup">
					{!! link_to_route('task.edit', 'Edit task', $arguments = [$task->id], $parameters = ["class"=>"btn btn-default editBtn"]) !!}
					<form action="{!! route('task.destroy',['id'=>$task->id]) !!} " method="POST" id="delForm">
						{!! csrf_field() !!}
						{!! method_field('delete') !!}
						<div class="form-group">
							<input type="submit" name="submit" value="delete" class="btn btn-danger delBtn">
						</div>
					</form>
				</div>
			@else	
				<p>No task found.</p>
			@endif
		</div>
	</div>
@endsection