<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Task extends Model{
	protected $fillable = ['task_name', 'task_content'];
	protected $hidden = ['_token'];
}