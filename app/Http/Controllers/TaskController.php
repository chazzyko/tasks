<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Task;
use DB;
use Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 protected $rules = [
			'task_name' => 'required',
			'task_content' => 'required'
		];
	 
    public function index()
    {
		$tasks = DB::table('tasks')->paginate(8);
		$count = 0;
		
        return view('showtasks')->with('title','All tasks')->with('tasks', $tasks)->with('count',$count);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('newtask')->with('title','New task');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
				
		
		$input = $request->all();
		//$input = $request->except('_token');
		
		$validator = Validator::make($input, $this->rules);
		
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		
        //$id = DB::table('tasks')->insertGetId($input);
		$task = new Task;
		$task->task_name = $request->input('task_name');
		$task->task_content = $request->input('task_content');
		$task->save();
		
		return redirect()->route('task.show',[$task->id])->with('title','A task');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$task = DB::table('tasks')->where('id','=', $id)->first();
				
        return view('showonetask')->with('title','Task')->with('task',$task);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = DB::table('tasks')->where('id','=', $id)->first();
		
		return view('edittask')->with('title','Edit task')->with('task',$task);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		//$task = DB::table('tasks')->where('id','=',$id)->first();
		$task = Task::find($id);
		$input = $request->all();
		
		$validator = Validator::make($input, $this->rules);
		if($validator->fails()) return back()->withInput()->withErrors($validator);
		// $task->task_name = strip_tags($request->task_name);
		// $task->task_content = strip_tags($request->task_content);
		$task->task_name = $request->input('task_name');
		$task->task_content = $request->input('task_content');
		$task->save();
		
		return redirect()->route('task.show',[$task->id])->with('title','Show task');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
		if(count($task) > 0){
			$task->delete();
			return redirect()->route('task.index')->with('message', 'Task deleted');
		}
		return back()->with('error', 'Not deleted');		
    }
}
